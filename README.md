# os-challenge-resub-1

# DTU OS Challenge
This project revolves around the creation of a specialized server that processes reverse hashing requests. The server's primary function is to invert the SHA256 hash algorithm, which is essentially a one-way function transforming arbitrary input messages into a fixed-size byte sequence or message digest. The server's key characteristic is its deterministic nature, ensuring that identical inputs always result in the same hash output.
## Author

Sergio Piqueras Moreno (s233204)



## Overview

This repository contains the code for the Operating Systems Challenge hosted by the Technical University of Denmark (DTU). It is structured into multiple branches, each serving a different phase or feature set of the challenge.



## Branches



### main (default)

The `main` branch is the default branch that typically contains the most stable version of the project. It includes the core functionality that is common across all features and is the foundation upon which other branches are built.



### Client

The `Client` branch is focused on the development of the client-side application. This includes all the necessary code to interface with the server, handle user inputs, and manage client-side operations like request generation and response handling.




### Starting the Server



To start the server, you will use the provided shell script that initializes the server with the necessary configurations.



1. Open a terminal.

2. Navigate to the directory containing the `run-reference-server.sh` script.

3. on the terminal:

   ./run-reference-server.sh

