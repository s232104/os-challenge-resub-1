#include <stdio.h>
#include <openssl/sha.h>
#include <string.h>
#include <endian.h>
#include <pthread.h>
#include "acc_prof.h"

#include <message/msg.h>
#include <message/messages.h>
#include "logic/invert_sha.h"

/**********************Basic server**********************/

void start(int port)
{
    printf("Starting program (using port %d)\n", port);
    init_server(port);

    for(;;){
        Packet p;
        get_packet(&p);
        uint64_t result;
        inv_hash(&result, p.hash, p.start, p.end);
        send_answer(result, p.connfd);
        printf("[server] %ld \n",result); // For see the result on the server.
    }

    close_server();
}
    
