#include "logic/invert_sha.h"
#include <openssl/sha.h>
#include <string.h>

// Contains for computing the inverse of SHA256.
 



 //Checks if a number computes the given SHA256 hash  1 for true. 0 for false.

int is_inv_hash(uint64_t num, unsigned char *hash)
{
    //Computes SHA256 for i.

    unsigned char md[SHA256_DIGEST_LENGTH];
    SHA256((unsigned char*)&num, sizeof(num), md);

    //Check if the result of SHA256 is the result being looked for.

    return memcmp(hash, md, SHA256_DIGEST_LENGTH) == 0;
}

//Single threaded brute force computation of the inverse SHA256 of the number in the range [from; to] 
  
int inv_hash(uint64_t* result, unsigned char *value, uint64_t from, uint64_t to)
{
    for (uint64_t i = from; i <= to; i++){
        if (is_inv_hash(i, value))
        {
            //The inverse hash is found.
            *result = i;
            return 1;
        }
    }
    //The inverse hash is not in [from; to].
    return 0;
}