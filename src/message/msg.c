#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>

//For the Auxiliary function
#include <errno.h>  
#include <signal.h> 

#include <message/messages.h>
#include <message/msg.h>

int sockfd = -1;
char buffer[PACKET_REQUEST_SIZE];

//Auxiliary function to read a complete socket package

ssize_t read_full_packet(int sockfd, char *buffer, size_t length) {
    ssize_t bytes_read;
    size_t total_read = 0;
    while (total_read < length) {
        bytes_read = read(sockfd, buffer + total_read, length - total_read);
        if (bytes_read < 0) {
            if (errno == EINTR) {
                continue; // The call was interrupted by a signal, continue reading
            }
            // An error occurred while reading
            return -1;
        }
        if (bytes_read == 0) {
            // The client closed the connection
            break;
        }
        total_read += bytes_read;
    }
    return total_read;
}


//Initializes the server.

void init_server(int port)
{
    printf("Initializing server...\n");

    //Creating the socket.
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in server_addr;
    if (sockfd < 0){
        printf("Error creating socket.\n");
        _exit(-1);
    }

    bzero((char *) (&server_addr), sizeof(server_addr));
    
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(port);

    //Binding.
    int bind_status = bind(sockfd, (struct sockaddr *)(&server_addr), sizeof(server_addr));
    if (bind_status < 0){
        printf("Error on binding.\n");
        _exit(-1);
    }

    listen(sockfd, 150); //Side of the backlog to overload on the server, allowing it to deal with temporary spikes in connection requests.

    printf("Initialized sucessfully.\n");
}

//For close the server 

int close_server()
{
    return shutdown(sockfd, 2);
}

//Interprets and stores the data in the buffer in the packet. 

void buf_to_packet(char buf[PACKET_REQUEST_SIZE], Packet *ret)
{
    memcpy(&(ret->hash), buf, 32);
    memcpy(&(ret->start), buf + 32, 8);
    memcpy(&(ret->end), buf + 40, 8);
    memcpy(&(ret->priority), buf + 48, 1);

    //Endian conversion.
    ret->start = be64toh(ret->start);
    ret->end = be64toh(ret->end);
}

// Accepts the next client and reads the packet sent.

void get_packet(Packet *ret) 
{
    struct sockaddr_in client_addr;
    unsigned int client_addr_len = sizeof(client_addr);
    int connfd = accept(sockfd, (struct sockaddr *)(&client_addr), &client_addr_len);
    if (connfd < 0) {
        perror("Error on accept the client");
        _exit(EXIT_FAILURE);
    }

    // Use the new incremental reading function
    if (read_full_packet(connfd, buffer, PACKET_REQUEST_SIZE) < PACKET_REQUEST_SIZE) {
        printf("Error reading full packet from socket or connection closed.\n");
        close(connfd);
        return;
    }

    buf_to_packet(buffer, ret);
    ret->connfd = connfd;
}
 //Sends the answer to the client

void send_answer(uint64_t answer, int connfd)
{
    //Covert to endian
    answer = htobe64(answer);
    if (write(connfd, &answer, 8) < 0)
        printf("Error sending message. \n");
    shutdown(connfd, 2);
}

