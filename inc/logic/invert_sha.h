#ifndef INVERT_SHA_H
#define INVERT_SHA_H

#include <stdint.h>

int is_inv_hash(uint64_t num, unsigned char *hash);

int inv_hash(uint64_t* result, unsigned char *value, uint64_t from, uint64_t to);

#endif